//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Shyam H Lad on 8/12/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import XCTest
@testable import NYCSchools

class NYCSchoolsTests: XCTestCase {

    func testFetchSchoolMetaData() throws {
        DataLoader.fetchSchoolMetaData(numItems: 50, offset: 0) { (schools, e) in
            XCTAssertNotNil(schools)
            XCTAssertEqual(schools?.count, 50, "The expected number of schools is 50")
            XCTAssertNotNil(schools![0].school_name)
            
        }
    }
    
    func testFetchFilteredSchoolMetaData() throws {
        DataLoader.fetchFilteredSchoolMetaData(name: "brook") { (schools, e) in
            XCTAssertNotNil(schools)
            XCTAssert(schools!.count > 0)
            XCTAssertNotNil(schools![0].school_name)
            
        }
    }
}
