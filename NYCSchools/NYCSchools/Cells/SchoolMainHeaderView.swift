//
//  SchoolMainHeaderView.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/11/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit

class SchoolMainHeaderView: UIView {
    
    var title: UILabel?
    var subHeader: UILabel?
    var titleName: String?
    var subHeaderText: String?
    var searchBar: UISearchBar?

    override init(frame: CGRect) {
       super.init(frame: frame)
       self.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
       layoutHeaderViews()
       layoutSearchBar()
       
    }

    required init?(coder: NSCoder) {
       fatalError("init(coder:) has not been implemented")
    }
    
    //This lays out the Title, subtitle, and search bar. The Search bar's functionality is handled in the main view controller 
    private func layoutHeaderViews() {
       let mainTitle = UILabel()
       self.addSubview(mainTitle)
       self.title = mainTitle
       mainTitle.translatesAutoresizingMaskIntoConstraints = false
       NSLayoutConstraint.activate([
           mainTitle.leftAnchor.constraint(equalTo: self.layoutMarginsGuide.leftAnchor),
           mainTitle.rightAnchor.constraint(equalTo: self.layoutMarginsGuide.rightAnchor),
           mainTitle.topAnchor.constraint(equalTo: self.layoutMarginsGuide.topAnchor),
           mainTitle.heightAnchor.constraint(equalToConstant: 90)
       ])
       mainTitle.font = UIFont(name: "Helvetica-Bold", size: 100)
       mainTitle.textColor = .black
       mainTitle.adjustsFontForContentSizeCategory = true
       mainTitle.textAlignment = .center
       mainTitle.isUserInteractionEnabled = false
       mainTitle.adjustsFontSizeToFitWidth = true
       mainTitle.minimumScaleFactor = 0.5
       
       let sub = UILabel()
       self.addSubview(sub)
       self.subHeader = sub
       sub.translatesAutoresizingMaskIntoConstraints = false
       NSLayoutConstraint.activate([
           sub.leftAnchor.constraint(equalTo: self.layoutMarginsGuide.leftAnchor),
           sub.rightAnchor.constraint(equalTo: self.layoutMarginsGuide.rightAnchor),
           sub.topAnchor.constraint(equalTo: mainTitle.bottomAnchor),
           sub.heightAnchor.constraint(equalToConstant: 20)
       ])
       sub.font = UIFont(name: "Helvetica", size: 15)
       sub.textColor = .gray
       sub.adjustsFontForContentSizeCategory = true
       sub.textAlignment = .center
       sub.isUserInteractionEnabled = false
       sub.adjustsFontSizeToFitWidth = true
       sub.minimumScaleFactor = 0.5
    }

    func layoutSearchBar() {

        let searchBar = UISearchBar()
        self.searchBar = searchBar
        searchBar.isTranslucent = true
        searchBar.placeholder = "Search School Name"
        searchBar.searchBarStyle = UISearchBar.Style.minimal
        self.addSubview(searchBar)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            searchBar.leftAnchor.constraint(equalTo: self.layoutMarginsGuide.leftAnchor),
            searchBar.rightAnchor.constraint(equalTo: self.layoutMarginsGuide.rightAnchor),
            searchBar.topAnchor.constraint(equalTo: self.subHeader!.bottomAnchor, constant: 10),
            searchBar.heightAnchor.constraint(equalToConstant: 40)
        ])

    }

    public func setTitleName(str: String) {
       self.titleName = str
       self.title?.text = str
    }

    public func setSubHeaderText(str: String) {
       self.subHeaderText = str
       self.subHeader?.text = str
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.endEditing(true)
    }
}
