//
//  SchoolCell.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/9/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit

class SchoolCell: UICollectionViewCell {
    var pic: UIImageView?
    var name: String?
    var location: String?
    var title: UILabel?
    var subtitle: UILabel?
    var school: SchoolMeta?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setCardUI()
        self.layoutPic()
        self.layoutTitles()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //Note: All of this is setup. There is one function in here that will async get a random picture from unsplashed. I wanted to get a picture of the school eihter from google or google maps street view, but you need to pay to use the api :(
    // MARK: - Setup
    private func layoutPic() {
        let newPic = UIImageView()
        self.pic = newPic
        newPic.layer.cornerRadius = 15
        newPic.backgroundColor = .black
        newPic.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(newPic)
        newPic.topAnchor.constraint(equalTo: self.layoutMarginsGuide.topAnchor).isActive = true
        newPic.bottomAnchor.constraint(equalTo: self.layoutMarginsGuide.bottomAnchor).isActive = true
        newPic.leftAnchor.constraint(equalTo: self.layoutMarginsGuide.leftAnchor).isActive = true
        newPic.widthAnchor.constraint(equalToConstant: 150).isActive=true
        newPic.scalesLargeContentImage = true
        newPic.clipsToBounds = true
    }
    
    private func layoutTitles() {
        guard let pic = self.pic else {
            NSLog("There was an error when laying out the titles, Pic could not be found!")
            return;
        }
        let mainTitle = UILabel()
        self.addSubview(mainTitle)
        self.title = mainTitle
        mainTitle.translatesAutoresizingMaskIntoConstraints = false
//        mainTitle.backgroundColor = .red
        mainTitle.leftAnchor.constraint(equalTo: pic.rightAnchor, constant: 10).isActive = true
        mainTitle.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        mainTitle.topAnchor.constraint(equalTo: self.layoutMarginsGuide.topAnchor, constant: 10).isActive = true
        mainTitle.bottomAnchor.constraint(equalTo: self.layoutMarginsGuide.centerYAnchor).isActive = true
        mainTitle.font = UIFont(name: "Helvetica-Bold", size: 15)
        mainTitle.textColor = .black
        mainTitle.adjustsFontForContentSizeCategory = true
        mainTitle.adjustsFontSizeToFitWidth = true
        mainTitle.minimumScaleFactor = 0.1
        mainTitle.textAlignment = .left
        mainTitle.isUserInteractionEnabled = false
        mainTitle.lineBreakMode = .byWordWrapping
        mainTitle.numberOfLines = 0
        
        
        let sub = UILabel(frame: .zero)
        self.addSubview(sub)
        self.subtitle = sub
        sub.translatesAutoresizingMaskIntoConstraints = false
//        sub.backgroundColor = .green
        sub.leftAnchor.constraint(equalTo: pic.rightAnchor, constant: 10).isActive = true
        sub.rightAnchor.constraint(equalTo: self.layoutMarginsGuide.rightAnchor).isActive = true
        sub.topAnchor.constraint(equalTo: mainTitle.bottomAnchor).isActive = true
        sub.bottomAnchor.constraint(equalTo: self.layoutMarginsGuide.bottomAnchor).isActive = true
        sub.font = UIFont(name: "Helvetica", size: 13)
        sub.adjustsFontForContentSizeCategory = true
        sub.adjustsFontSizeToFitWidth = true
        sub.minimumScaleFactor = 0.1
        sub.isUserInteractionEnabled = false
        sub.textColor = .gray
        sub.lineBreakMode = .byWordWrapping
        sub.numberOfLines = 0
    }
    
    override func prepareForReuse() {
        self.pic!.image = UIImage()
        self.title?.text = ""
        self.subtitle?.text = ""
    }
    
    
    // MARK: - Public funcs
    
    public func initWithSchool(school: SchoolMeta) {
        self.setName(str: school.school_name!)
        self.setLocation(str: school.location!)
        self.school = school
    }
    public func setName(str: String) {
           self.name = str
           self.title!.text = str
       }
    
    public func setLocation(str: String) {
        let str = String(str.split(separator: "(")[0])
        self.location = str
        self.subtitle!.text = str
    }
    
    
    //async get a random pic 
    public func getImageForSchool() {
        DispatchQueue.global(qos: .background).async {
            let url = URL(string:"https://picsum.photos/500")
            if let data = try? Data(contentsOf: url!)
            {
                let image: UIImage = UIImage(data: data) ?? UIImage()
                DispatchQueue.main.async {
                     self.pic?.image = image
                }
            }
            
        }
    }
}
