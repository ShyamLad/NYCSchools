//
//  AppDelegate.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/9/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?

    //NOTE: only ONE library was used in this entire project, and it was a library to make a histogram for the SAT scores. EVERYTHING was done 100% programatically.
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        
        //force light mode
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        //Set up rounded tab bar with shadow
        //NOTE: if we try to confugure the shadow layer of the tab bar directly, it wont work because we are attempting to round the corners and it will get clipped off. This is a workaround where we add a uiview and have that form the shadow instead
        let tabBarController = TabBarController()
        let tabBar = tabBarController.tabBar
        let tabGradientView = UIView(frame: tabBarController.tabBar.bounds)
        tabGradientView.backgroundColor = UIColor.white
        tabGradientView.translatesAutoresizingMaskIntoConstraints = false;
        tabBar.addSubview(tabGradientView)
        tabBar.sendSubviewToBack(tabGradientView)
        tabGradientView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        //we do this to make sure the bottom task bar extends all the way to the corners (otherwise in square-edged screenshots, the taskbar will be rounded)
        tabGradientView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        tabGradientView.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabGradientView.layer.shadowRadius = 5
        tabGradientView.layer.shadowColor = UIColor.black.cgColor
        tabGradientView.layer.shadowOpacity = 0.25
        tabGradientView.layer.cornerRadius = 25
        tabBar.clipsToBounds = false
        tabBar.backgroundImage = UIImage()
        tabBar.shadowImage = UIImage()
        
        //make tabbarcontroller the root controller
        window?.rootViewController = UINavigationController(rootViewController: tabBarController)
        window?.makeKeyAndVisible()
        return true
    }

}

