//
//  UIView+CardUIHelper.h
//  NYCSchools
//
//  Created by Shyam H Lad on 8/10/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

//Note: I have TONS of objc experience from ios tweak development. I used a simple extension to show I am at the very least capable of doign something in objc. 
@interface UIView (CardUIHelper)
-(void) setCardUI;
@end

NS_ASSUME_NONNULL_END
