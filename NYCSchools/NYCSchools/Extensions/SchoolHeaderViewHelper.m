//
//  SchoolHeaderViewHelper.m
//  NYCSchools
//
//  Created by Shyam H Lad on 8/10/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MKMapView+SchoolHeaderViewHelper.h"

@implementation MKMapView (SchoolHeaderViewHelper)
-(void) centerToLocation: (CLLocation *) location regionalRadius: (CLLocationDistance *) regionalRadius {
    MKCoordinateRegion * coordinateRegion = MKCoordinateRegionMakeWithDistance([location coordinate], *regionalRadius, *regionalRadius);
    let coordinateRegion = MKCoordinateRegion(
        center: location.coordinate,
        latitudinalMeters: regionRadius,
        longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
}


@end
