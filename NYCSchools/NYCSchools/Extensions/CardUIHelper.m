//
//  CardUIHelper.m
//  NYCSchools
//
//  Created by Shyam H Lad on 8/10/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIView+CardUIHelper.h"

@implementation UIView (CardUIHelper)

//this sets the basics for our simplistic card ui theme
-(void) setCardUI {
    [self setBackgroundColor:UIColor.whiteColor];
    CALayer* layer = [self layer];
    [layer setCornerRadius:15];
    [layer setShadowColor:UIColor.blackColor.CGColor];
    [layer setShadowOpacity:0.25f];
    [layer setShadowRadius:5];
}

@end
