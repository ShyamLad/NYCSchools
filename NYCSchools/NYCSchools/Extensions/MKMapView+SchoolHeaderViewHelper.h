//
//  MKMapView+SchoolHeaderViewHelper.h
//  NYCSchools
//
//  Created by Shyam H Lad on 8/10/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MKMapView (SchoolHeaderViewHelper)
-(void) centerToLocation: (CLLocation *) location regionalRadius: (CLLocationDistance *) regionalRadius;
@end

NS_ASSUME_NONNULL_END
