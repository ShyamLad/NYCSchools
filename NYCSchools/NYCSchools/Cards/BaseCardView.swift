//
//  BaseCardView.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/10/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit
import MapKit

//NOTE: This is the main card class that lays out the fundementals for all the cards in the drilldown view
class BaseCardView: UIView {
    
    
    var title: UILabel?
    var subHeader: UILabel?
    var titleName: String?
    var subHeaderText: String?
    var contentView: UIView?
    var school: SchoolMeta?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setCardUI()
        self.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        layoutHeaderViews()
        layoutContentView()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func layoutHeaderViews() {
        let mainTitle = UILabel()
        self.addSubview(mainTitle)
        self.title = mainTitle
        mainTitle.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainTitle.leftAnchor.constraint(equalTo: self.layoutMarginsGuide.leftAnchor),
            mainTitle.rightAnchor.constraint(equalTo: self.layoutMarginsGuide.rightAnchor),
            mainTitle.topAnchor.constraint(equalTo: self.layoutMarginsGuide.topAnchor),
            mainTitle.heightAnchor.constraint(equalToConstant: 40)
        ])
        mainTitle.font = UIFont(name: "Helvetica-Bold", size: 35)
        mainTitle.textColor = .black
        mainTitle.adjustsFontForContentSizeCategory = true
        mainTitle.textAlignment = .center
        mainTitle.isUserInteractionEnabled = false
        mainTitle.adjustsFontSizeToFitWidth = true
        mainTitle.minimumScaleFactor = 0.5
        
        let sub = UILabel()
        self.addSubview(sub)
        self.subHeader = sub
        sub.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sub.leftAnchor.constraint(equalTo: self.layoutMarginsGuide.leftAnchor),
            sub.rightAnchor.constraint(equalTo: self.layoutMarginsGuide.rightAnchor),
            sub.topAnchor.constraint(equalTo: mainTitle.bottomAnchor),
            sub.heightAnchor.constraint(equalToConstant: 20)
        ])
        sub.font = UIFont(name: "Helvetica", size: 15)
        sub.textColor = .gray
        sub.adjustsFontForContentSizeCategory = true
        sub.textAlignment = .center
        sub.isUserInteractionEnabled = false
        sub.adjustsFontSizeToFitWidth = true
        sub.minimumScaleFactor = 0.5
    }
    
    private func layoutContentView() {
        let content = UIView()
        self.contentView = content
        self.addSubview(content)
        content.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            content.leftAnchor.constraint(equalTo: self.layoutMarginsGuide.leftAnchor),
            content.rightAnchor.constraint(equalTo: self.layoutMarginsGuide.rightAnchor),
            content.topAnchor.constraint(equalTo: self.subHeader!.bottomAnchor, constant: 10),
            content.bottomAnchor.constraint(equalTo: self.layoutMarginsGuide.bottomAnchor)
        ])
    }
    
    public func setTitleName(str: String) {
        self.titleName = str
        self.title?.text = str
    }
    
    public func setSubHeaderText(str: String) {
        self.subHeaderText = str
        self.subHeader?.text = str
    }
    
    public func addToContentView(view: UIView) {
        guard let contentView = self.contentView else {
            return
        }
        contentView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
         NSLayoutConstraint.activate([
                   view.leftAnchor.constraint(equalTo: contentView.leftAnchor),
                   view.rightAnchor.constraint(equalTo: contentView.rightAnchor),
                   view.topAnchor.constraint(equalTo: contentView.topAnchor),
                   view.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
               ])
    }
    
    public func setSchool(school: SchoolMeta?) {
        self.school = school
    }

}

//For the map views, we want to center the location onto the school. Luckily, the data provides longitude and latitude
public extension MKMapView {
  func centerToLocation(
    _ location: CLLocation,
    regionRadius: CLLocationDistance = 1000
  ) {
    let coordinateRegion = MKCoordinateRegion(
      center: location.coordinate,
      latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}

//(continued form above) However, the way they provide latituide and longidude in the school meta data is in one string, allong with the address. This little function helps extract the information we need
public extension String {
    func slice(from: String, to: String) -> String? {
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                substring(with: substringFrom..<substringTo)
            }
        }
    }
}

//generic pin for the map. Could have put this in its own file but wasnt used anywhere else
class MapPin : MKPointAnnotation {

    init(coordinate: CLLocationCoordinate2D, title: String, subtitle: String) {
        super.init()
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
}
