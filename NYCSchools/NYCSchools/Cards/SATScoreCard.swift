//
//  SATScoreCard.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/10/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit
import Charts

class SATScoreCard: BaseCardView {
    
    var chartView: HorizontalBarChartView?
    var noDataLabel: UILabel?
    
    //NOTE: In here is the **ONLY** Library I have used for this entire project. This is to make the SAT bar chart using iOS Charts
    override func setSchool(school: SchoolMeta?) {
        super.setSchool(school: school)
        DataLoader.fetchSATScoresForSchool(id: school!.id) { (score, error) in
             DispatchQueue.main.async {
                if score != nil && !score!.isEmpty {
                    let chartView = HorizontalBarChartView()
                    self.addToContentView(view: chartView)
                    let subjects = ["Reading", "Writing", "Math"]
                    let reading = BarChartDataEntry(x: 0, y: Double(score![0].reading)!)
                    let writing = BarChartDataEntry(x: 1, y: Double(score![0].writing)!)
                    let math = BarChartDataEntry(x: 2, y: Double(score![0].math)!)
                    let dataSet = BarChartDataSet(entries: [reading, writing, math], label: "Examination Subjects")
                    dataSet.colors = ChartColorTemplates.colorful()
                    let data = BarChartData(dataSets: [dataSet])
                    chartView.data = data
                    chartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: subjects)
                    chartView.xAxis.granularity = 1
                    chartView.leftAxis.axisMinimum = 0
                    chartView.leftAxis.axisMaximum = 800
                    chartView.leftAxis.granularityEnabled = false
                    chartView.xAxis.drawGridLinesEnabled = false
                    chartView.rightAxis.enabled = false
                    chartView.notifyDataSetChanged()
                    chartView.isUserInteractionEnabled = false
                    self.chartView = chartView
                } else {
                    let noDataLab = UILabel()
                    self.noDataLabel = noDataLab
                    self.addToContentView(view: noDataLab)
                    noDataLab.font = UIFont.systemFont(ofSize: 25)
                    noDataLab.textAlignment = .center
                    noDataLab.text = "No Data Available"
                    noDataLab.textColor = .gray
                }
            }
        }
    }

}
