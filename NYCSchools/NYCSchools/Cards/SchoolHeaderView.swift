//
//  SchoolHeaderView.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/10/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit
import MapKit

class SchoolHeaderView: BaseCardView {

    var mapView: MKMapView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSchool(school: SchoolMeta?) {
        guard let school = school else { return }
        super.setSchool(school: school)
        self.setTitleName(str: school.school_name!)
        self.setSubHeaderText(str: school.location!)
        layoutMapView()
    }
    
    //simply lay out map view with one pin as the school. Make it noninteractive
    private func layoutMapView() {
        let mv = MKMapView()
        self.mapView = mv
        mv.layer.cornerRadius = 10
        self.addToContentView(view: mv)
        mv.isUserInteractionEnabled = false
        // Set initial location in Honolulu
        let loc = school!.location!.slice(from: "(", to: ")")?.replacingOccurrences(of: " ", with: "").split(separator: ",")
        let lat = Double(String(loc![0]))!
        let long = Double(String(loc![1]))!
        let initialLocation = CLLocation(latitude: lat, longitude: long)
        mv.addAnnotation(MapPin(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long), title: school!.school_name!, subtitle: school!.location!))
        mv.centerToLocation(initialLocation)
    }
}
