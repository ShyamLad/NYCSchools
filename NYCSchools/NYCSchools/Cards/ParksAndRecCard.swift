//
//  ParksAndRecCard.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/10/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit
import MapKit

class ParksAndRecCard: BaseCardView {
    
    var parks: [Park]?
    var mapView: MKMapView?

    override func setSchool(school: SchoolMeta?) {
        super.setSchool(school: school)
        layoutMapView()
    }
    
    private func layoutMapView() {
        let mv = MKMapView()
        self.mapView = mv
        mv.layer.cornerRadius = 10
        self.addToContentView(view: mv)
        mv.isUserInteractionEnabled = false
        let loc = school!.location!.slice(from: "(", to: ")")?.replacingOccurrences(of: " ", with: "").split(separator: ",")
        let lat = Double(String(loc![0]))!
        let long = Double(String(loc![1]))!
        let initialLocation = CLLocation(latitude: lat, longitude: long)
        mv.addAnnotation(MapPin(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long), title: school!.school_name!, subtitle: school!.location!))
        mv.centerToLocation(initialLocation)
        mv.isUserInteractionEnabled = true
        //get the boro. There is a data discrepency where brooklyn will show up as K when it should be B
        let boro = school!.boro! == "K" ? "B" : school!.boro!
        //We can layout all the parks as pins in the map view
        DataLoader.fetchParksForBorough(boro) { (parks, e) in
            guard let parks = parks else { return }
            for park in parks {
                 DispatchQueue.main.async {
                    let closure = park.status ?? "UNKNOWN"
                    let name = park.name ?? "UNKNOWN NAME"
                    guard let long = park.point!.coordinates?[0], let lat = park.point!.coordinates?[1] else { return }
                    mv.addAnnotation(MapPin(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long), title: name, subtitle: closure))
                }
            }
        }
    }

}
