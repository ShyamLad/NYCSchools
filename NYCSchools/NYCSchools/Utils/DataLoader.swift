//
//  DataLoader.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/10/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit


class DataLoader: NSObject {
    
    //NOTE: In hindsight, i could have made one function that generically gets data and have an enum request param that designates what type of fetch i am doing. Instead, i made one for every type of request i had. All of these are asycn and have escaping completion blocks. The NYC data is SoQL, so it makes it easy to query. We can almost do full SQL queries to get what we want, makes it much easier and removes the need to have some type of persistent data system (i.e. CoreData)
    
    public static func fetchSchoolMetaData(numItems: Int, offset: Int, completion: @escaping ([SchoolMeta]?, Error?) -> Void) {
        let queryItems = [URLQueryItem(name: "$limit", value: String(numItems)), URLQueryItem(name: "$offset", value: String(offset))]
        var urlComps = URLComponents(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
        urlComps.queryItems = queryItems
        let url = urlComps.url!
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let schools: [SchoolMeta] = try! JSONDecoder().decode([SchoolMeta].self, from: data)
            completion(schools, nil)
        }
        task.resume()
    }
    
    public static func fetchFilteredSchoolMetaData(name: String, completion: @escaping ([SchoolMeta]?, Error?) -> Void) {
        let queryItems = [URLQueryItem(name: "$where", value: "UPPER(school_name) like UPPER('%" + name + "%')")]
        var urlComps = URLComponents(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!
        urlComps.queryItems = queryItems
        let url = urlComps.url!
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let schools: [SchoolMeta] = try! JSONDecoder().decode([SchoolMeta].self, from: data)
            completion(schools, nil)
        }
        task.resume()
    }
    
    public static func fetchSATScoresForSchool(id: String, completion: @escaping ([SATScores]?, Error?) -> Void) {
        let queryItems = [URLQueryItem(name: "dbn", value: id)]
        var urlComps = URLComponents(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!
        urlComps.queryItems = queryItems
        let url = urlComps.url!
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let scores: [SATScores] = try! JSONDecoder().decode([SATScores].self, from: data)
            completion(scores, nil)
        }
        task.resume()
    }
    
    public static func fetchParksForBorough(_ borough: String, completion: @escaping ([Park]?, Error?) -> Void) {
        let queryItems = [URLQueryItem(name: "borough", value: borough)]
        var urlComps = URLComponents(string: "https://data.cityofnewyork.us/resource/a4qt-mpr5.json")!
        urlComps.queryItems = queryItems
        let url = urlComps.url!
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            let parks: [Park] = try! JSONDecoder().decode([Park].self, from: data)
            completion(parks, nil)
        }
        task.resume()
    }
}
