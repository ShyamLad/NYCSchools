//
//  DataEntities.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/10/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit


//some general entities that are used when retunring the fetched data. All the data is decoded and mapped into these structs.

struct SchoolMeta: Decodable {
    enum CodingKeys: String, CodingKey {
        case boro, location, phone_number, school_email, school_name, website
        case id = "dbn"
        case description = "overview_paragraph"
    }
    let id: String
    let boro: String?
    let location: String?
    let description: String?
    let phone_number: String?
    let school_email: String?
    let school_name: String?
    let website: String?
}

struct SATScores: Decodable {
    enum CodingKeys: String, CodingKey {
        case reading = "sat_critical_reading_avg_score"
        case math = "sat_math_avg_score"
        case writing = "sat_writing_avg_score"
        case totalTested = "num_of_sat_test_takers"
    }
    let reading: String
    let writing: String
    let math: String
    let totalTested: String
    
}

struct Park: Decodable {
    enum CodingKeys: String, CodingKey {
        case name, borough, location, status, point
    }
    let name: String?
    let borough: String?
    let location: String?
    let status: String?
    let point: Point?
}

struct Point: Decodable {
    enum CodingKeys: String, CodingKey {
        case coordinates
    }
    let coordinates: [Double]?
}
