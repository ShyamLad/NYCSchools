//
//  SchoolMainViewController.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/9/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit

class SchoolMainViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UISearchBarDelegate, UITextFieldDelegate {
    

    var schoolCollectionView: UICollectionView?
    var schoolHeaderView: SchoolMainHeaderView?
    var schoolMetaData: [SchoolMeta] = Array()
    var filteredSchools: [SchoolMeta]?
    var offset: Int = 0
    var pageSize: Int = 50
    var fetching: Bool = false
    var full: Bool = false

    
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        //Fetch data first in order to populate collection view. Data fetches are lazy and paginated
        self.fetchNextDataSet()
        self.initSchoolCollectionView()
        self.initSchoolHeaderView()
    }
    
    // Note: we don't use a real header here. Instead, I made my own view and added it to the VC's view, and then set a content inset of 175 in the collection view. This gives it the appearence of being a header
    private func initSchoolHeaderView() {
        let header = SchoolMainHeaderView()
        self.schoolHeaderView = header
        self.view.addSubview(header)
        header.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            header.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor, constant: -75),
            header.leftAnchor.constraint(equalTo: self.view.layoutMarginsGuide.leftAnchor),
            header.rightAnchor.constraint(equalTo: self.view.layoutMarginsGuide.rightAnchor),
            header.heightAnchor.constraint(equalToConstant: 200)
               ])
        header.setTitleName(str: "NYCSchools")
        header.setSubHeaderText(str: "Select a school to see SAT Scores and nearby COVID-19 park closures!")
        header.searchBar!.delegate = self
        header.searchBar!.searchTextField.delegate = self
    }
    
    private func initSchoolCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        //add a content inset of 175 from the top to accomedate for the header search bar
        layout.sectionInset = UIEdgeInsets(top: 175, left: 10, bottom: 10, right: 10)
        let width = view.frame.size.width - layout.sectionInset.left - layout.sectionInset.right
        layout.itemSize = CGSize(width: width, height: 150)
        layout.minimumLineSpacing = 15
        let colllectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        self.schoolCollectionView = colllectionView
        colllectionView.register(SchoolCell.self, forCellWithReuseIdentifier: "SchoolCell")
        colllectionView.backgroundColor = UIColor.white
        self.view.addSubview(colllectionView)
        colllectionView.dataSource = self
        colllectionView.delegate = self
    }
    
    //This method is how we get our next set of results. It asynchronously fetches data and calls for a collectionview refresh.
    // Hacky solution: in order to prevent fetching data multiple times, I implemented a sort of "lock" by setting fetching=true when the data is being fetched. This lock is checked later when the collectionview is dequeuing
    private func fetchNextDataSet() {
        fetching = true
        DataLoader.fetchSchoolMetaData(numItems: pageSize, offset: offset) { (schools, error) in
            guard let schools = schools else { return }
            //if the numbe of items returned is less than the page size, then we have gotten all the data
            if schools.count < self.pageSize {
                self.full = true
            }
            self.schoolMetaData += schools
            self.offset += schools.count
            //Since DataLoader calls async fetch in global queue, we need to call the reload in the main queue
            DispatchQueue.main.async {
                self.fetching = false
                self.schoolCollectionView?.reloadData()
            }
        }
    }
    
    // MARK: - SchoolCollectionViewController
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //If the user is searching using the search bar and filtered is populated, use that as the count instead
        guard let filtered = filteredSchools else {
            return schoolMetaData.count
        }
        return filtered.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SchoolCell", for: indexPath) as! SchoolCell
        //if filtered is not nill (whcih means the user has inputted text to the search bar), use it as the data source. Otherwise, use schoolMetaData
        guard let filtered = self.filteredSchools else {
            if fetching {
                return cell
            }
            //prefetch when we are close to the next end of the current set. Only fetch if we have not gotten the full set of data
            if indexPath.row >= schoolMetaData.count - 25 && !full {
                fetchNextDataSet()
            } else {
                cell.initWithSchool(school: self.schoolMetaData[indexPath.row])
                cell.getImageForSchool()
            }
            return cell
        }
        cell.initWithSchool(school: filtered[indexPath.row])
        cell.getImageForSchool()
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        view.endEditing(true)
        let drillDown = SchoolDrillDownViewController()
        let selected = collectionView.cellForItem(at: indexPath) as! SchoolCell
        guard let school = selected.school else { return }
        drillDown.initWithSchool(school: school)
        navigationController?.pushViewController(drillDown, animated: true)
    }
    
    //this is purely to have the cool Kobe fade away for the search and title header when the user scrolls up in the collectionview
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        if (contentOffset <= 0 || self.schoolHeaderView!.alpha > 0.0) {
            UIView.animate(withDuration: 0.1) {
                self.schoolHeaderView?.alpha = CGFloat(contentOffset / -44)
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    // MARK: - Search Bar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.searchFilteredData), object: nil)
        self.perform(#selector(self.searchFilteredData), with: nil, afterDelay: 0.5)
    }
    
    @objc func searchFilteredData() {
        guard let searchText = self.schoolHeaderView?.searchBar?.text else { return }
        if searchText.isEmpty {
            filteredSchools = nil
            self.schoolCollectionView?.reloadData()
        }
        //fetch the filtered data
        DataLoader.fetchFilteredSchoolMetaData(name: searchText) { (filtered, e) in
            self.filteredSchools = filtered ?? []
             DispatchQueue.main.async {
                self.schoolCollectionView?.reloadData()
            }
        }
    }
    
    //make sure the keyboard gets removed after hitting search
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           textField.resignFirstResponder()
           return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }

}
