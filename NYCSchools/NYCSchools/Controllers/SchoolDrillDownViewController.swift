//
//  SchoolDrillDownViewController.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/10/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit

class SchoolDrillDownViewController: UIViewController, UIScrollViewDelegate {

    var scrollView: UIScrollView?
    var school: SchoolMeta?
    
    //have our own init (illegitimate) that sets up the subviews.
    // NOTE: I Chose NOT to use a collectionview, but rather a scrollview with a stackview in it. I did this becuase i wanted many different types of views being displayed. In hindsight, i may have been able to split the collection view up into sections and each section have its own unique cell, but this was kinda cool to do.
    public func initWithSchool(school: SchoolMeta) {
        self.school = school
        self.view.backgroundColor = .white
        //first we need to layout our scrollview that will take up the entire screen
        let sv = UIScrollView()
        self.scrollView = sv
        self.view.addSubview(sv)
        sv.contentInset = UIEdgeInsets(top: 25, left: 0, bottom: 0, right: 0)
        sv.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
          sv.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
          sv.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
          sv.topAnchor.constraint(equalTo: self.view.topAnchor),
          sv.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
          sv.leftAnchor.constraint(equalTo: self.view.leftAnchor),
          sv.rightAnchor.constraint(equalTo: self.view.rightAnchor)
        ])
        layoutScrollViewSubviews()
    }
    
    //now we can layout our stackview, and have each view be put into the stackview.
    private func layoutScrollViewSubviews() {
        guard let scrollView = self.scrollView else {
            return
        }
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 25
        stackView.distribution = .fill
        stackView.layoutMargins = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        scrollView.addSubview(stackView)

        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            // Attaching the content's edges to the scroll view's edges
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        
        //NOTE: all of these cards are extensions of BaseCardView. This keeps it easy to layout the title, subtitle, and card theme
        let headerView = SchoolHeaderView()
        stackView.addArrangedSubview(headerView)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.leftAnchor.constraint(equalTo: stackView.layoutMarginsGuide.leftAnchor).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 400).isActive = true
        headerView.setSchool(school: self.school)
        
        let satScoreCard = SATScoreCard()
        satScoreCard.backgroundColor = .white
        satScoreCard.setCardUI()
        stackView.addArrangedSubview(satScoreCard)
        satScoreCard.translatesAutoresizingMaskIntoConstraints = false
        satScoreCard.heightAnchor.constraint(equalToConstant: 500).isActive = true
        satScoreCard.leftAnchor.constraint(equalTo: stackView.layoutMarginsGuide.leftAnchor).isActive = true
        satScoreCard.setTitleName(str: "SAT Scores")
        satScoreCard.setSubHeaderText(str: "The average SAT score per examination area")
        satScoreCard.setSchool(school: school)
        
        let parksCard = ParksAndRecCard()
        parksCard.backgroundColor = .white
        parksCard.setCardUI()
        stackView.addArrangedSubview(parksCard)
        parksCard.translatesAutoresizingMaskIntoConstraints = false
        parksCard.heightAnchor.constraint(equalToConstant: 650).isActive = true
        parksCard.leftAnchor.constraint(equalTo: stackView.layoutMarginsGuide.leftAnchor).isActive = true
        parksCard.setTitleName(str: "COVID-19 Park Status")
        parksCard.setSubHeaderText(str: "Tap on a park pin to see if its open due to COVID-19")
        parksCard.setSchool(school: school)
            

    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
