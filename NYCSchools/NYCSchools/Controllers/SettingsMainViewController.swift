//
//  SettingsMainViewController.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/9/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit

class SettingsMainViewController: UIViewController {
    
    var noDataLabel: UILabel?

    //NOTE: I added a setttings page, but did not find any necessary settings to put in....  :)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        
        let lbl = UILabel()
        self.noDataLabel = lbl
        lbl.font = UIFont.systemFont(ofSize: 25)
        lbl.textAlignment = .center
        lbl.text = "Settings coming soon! 😬"
        lbl.textColor = .gray
        self.view.addSubview(lbl)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
                  lbl.leftAnchor.constraint(equalTo: view.leftAnchor),
                  lbl.rightAnchor.constraint(equalTo: view.rightAnchor),
                  lbl.topAnchor.constraint(equalTo: view.topAnchor),
                  lbl.bottomAnchor.constraint(equalTo: view.bottomAnchor)
              ])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
