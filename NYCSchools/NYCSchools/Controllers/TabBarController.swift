//
//  TabBarController.swift
//  NYCSchools
//
//  Created by Shyam H Lad on 8/9/20.
//  Copyright © 2020 Shyam H Lad. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //Set up the two root view controlelrs, give each one generic names to save time
        let firstViewController = SchoolMainViewController()
        firstViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)

        let secondViewController = SettingsMainViewController()
        secondViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .more, tag: 1)

        let tabBarList = [firstViewController, secondViewController]
        viewControllers = tabBarList
    }
    
    
    //we hide the nav bar since in the main VC
    override func viewWillAppear(_ animated: Bool) {
           super.viewWillAppear(animated)

           self.navigationController?.setNavigationBarHidden(true, animated: animated)
       }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

}
