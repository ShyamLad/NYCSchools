# NYC Schools
### by Shyam Lad

## Features
- **fully programatically constructed, including UI**
- only used **1** library for a histogram
- implements lazy loading of data
- in addition to school SAT scores, this app shows an interactive map of nearby park repopenign statuses due to COVID-19
- created original generic template for "card" - like UI
- **reactive** searching

## Future improvements
- connect with google images or google street view in order to obtain image of each school (currently it only displays a random image)
- proper universal data management
- settings panel
- more school stats